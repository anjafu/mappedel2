package ntnu.idatt2001.anjafu.mappeDel2.model;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * factory class that creates different node objects
 */
public class NodeFactory {
    /**
     * method that creates a chosen node object
     * @param nodeName - the name of the node object that is requested
     * @return a new node object that was requested
     */
    public static Node create(String nodeName) {
        if (nodeName.isBlank()) {
            return null;
        }else if(nodeName.equalsIgnoreCase("tableview")){
            return new TableView<>();
        }else if(nodeName.equalsIgnoreCase("menubar")){
            return new MenuBar();
        }else if(nodeName.equalsIgnoreCase("toolbar")){
            return new ToolBar();
        }else if(nodeName.equalsIgnoreCase("button")){
            return new Button();
        }else if(nodeName.equalsIgnoreCase("borderpane")){
            return new BorderPane();
        }else if(nodeName.equalsIgnoreCase("vbox")){
            return new VBox();
        }else if(nodeName.equalsIgnoreCase("gridpane")){
            return new GridPane();
        }else if(nodeName.equalsIgnoreCase("textfield")){
            return new TextField();
        }
        return null;
    }
}
