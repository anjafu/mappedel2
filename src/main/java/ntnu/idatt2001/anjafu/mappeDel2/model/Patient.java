package ntnu.idatt2001.anjafu.mappeDel2.model;

import java.util.Objects;

/**
 * class that represents one patient
 * A patient has a social security number, a name (first and last), a diagnosis and a general practitioner
 */
public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * constructor that creates a new patient
     * @param socialSecurityNumber - the persons social security number
     * @param firstName - the persons first name
     * @param lastName - the persons last name
     * @param generalPractitioner - the persons general practitioner
     * @param diagnosis - the persons diagnosis
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName, String generalPractitioner, String diagnosis) {
        //checking whether the input is valid
        if(firstName.isBlank() || lastName.isBlank()){
            throw new IllegalArgumentException("A patient must have a name.");
        }else if(socialSecurityNumber.isBlank()){
            throw new IllegalArgumentException("A patient must have a social security number.");
        }
        //ignoring the fact that social security number has to be 11 digits long, since
        //the Patients.csv file contained patients with only 10 digit long social security numbers
        /*else if(!(socialSecurityNumber.trim().length() == 11)){
            throw new IllegalArgumentException("Social security numbers are 11 digits long");
        }*/
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.generalPractitioner = generalPractitioner;
        this.diagnosis = diagnosis;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstname(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    public String getFullName(){
        return firstName + " " + lastName;
    }

    /**
     * method that returns all the information belonging to a given patient
     * @return all the contents to the patient
     */
    @Override
    public String toString() {
        return "socialSecurityNumber: " + socialSecurityNumber +
                "\n, name: " + firstName + " " + lastName +
                "\n, diagnosis: " + diagnosis +
                "\n, generalPractitioner: " + generalPractitioner;
    }

    /**
     * method that checks whether or not two patient objects are the same. They are the same if either:
     * 1. the security numbers of both objects are the same
     * 2. both the objects are the same
     * @param o - the other object you want to compare with
     * @return true if the patient objects are the same or false if they are not
     */
    @Override
    public boolean equals(Object o) {
        //checks if both of the objects point to the same object (and therefore are the same)
        if (this == o) return true;
        //checks if the object o is empty/null or a different type than department
        if (!(o instanceof Patient)) return false;
        //know now that the object is of the patient type
        Patient patient = (Patient) o;
        //checks whether the social security numbers of both the patients are the same
        return socialSecurityNumber.equalsIgnoreCase(patient.getSocialSecurityNumber());
    }

    /**
     * method that creates a unique id for a patient object
     * @return the unique id for a patient
     */
    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }
}
