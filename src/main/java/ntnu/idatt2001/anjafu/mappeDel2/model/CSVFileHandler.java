package ntnu.idatt2001.anjafu.mappeDel2.model;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * class that handles reading and writing csv files
 */
public class CSVFileHandler {
    /**
     * method that reads csv files and adds the patient to the register
     * @param file - the file that is going to be read
     * @param register - the register the patients read from the csv file shall be added to
     * @throws IOException
     */
    public static void readCSVFile(File file, PatientRegister register) throws IOException{
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                        new FileInputStream(file), StandardCharsets.UTF_8));
       //emptying the register so it only contains information from the file
       register.getPatients().clear();

       String line;
       //reading the first line in the file, since it contains info about the file, not a patient
       bufferedReader.readLine();
       while ((line = bufferedReader.readLine()) != null) {
           //splitting the line into an array to get all the information seperated
           String[] patientInfo = line.split(";");
           //getting the patient information from the array
           //setting diagnosis as empty since diagnosis is not saved in Patients.csv file
           Patient p = new Patient(patientInfo[3],patientInfo[0],patientInfo[1],patientInfo[2],"");
           //adding the patient to the register
           register.addPatient(p);
       }
    }

    /**
     * method that fills a file up with information from a patient register
     * @param file - the file that shall be filled with the information
     * @param register - the register that contains the information the file needs
     * @throws IOException
     */
    public static void writeCSVFile(File file, PatientRegister register) throws IOException{
        //changed to BufferedWriter instead of FileWriter since running the program
        //from maven - plugins - javafx:run caused the program to not be able to read "æ,ø,å"
        //therefore changed to BufferedWriter to directly tell what encoding should be used
        BufferedWriter fileWriter = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(file),StandardCharsets.UTF_8));
        //writing the first line in the file to contain info about the syntax of the following lines
        fileWriter.write("firstName;lastNam;generalPractitioner;socialSecurityNumber\n");

        //iterating through the register
        for (Patient p: register.getPatients()) {
            //filling the file with information about the patient
            fileWriter.write(p.getFirstName()+";"+p.getLastName()+";"+p.getGeneralPractitioner()+";"
                        +p.getSocialSecurityNumber()+"\n");
            //not saving the diagnosis since the Patients.csv file does not, and diagnosis is
            //something that can change over the time, so its not really necessary to keep
        }

        //closing the file writer when its done iterating through the register
        fileWriter.close();

    }
}
