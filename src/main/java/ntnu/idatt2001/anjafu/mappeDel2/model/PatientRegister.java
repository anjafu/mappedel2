package ntnu.idatt2001.anjafu.mappeDel2.model;

import java.util.ArrayList;

/**
 * class that represents one patient register
 */
public class PatientRegister {
    private ArrayList<Patient> allPatients;

    /**
     * constructor to create a new patient register
     */
    public PatientRegister() {
        this.allPatients = new ArrayList<>();
    }

    /**
     * method to get all the patients belonging to a given patient register
     * @return a list of all patients
     */
    public ArrayList<Patient> getPatients() {
        return allPatients;
    }

    /**
     * method to add a new patient to the patient register
     * @param patient - the new patient
     * @return true if the patient was not registered already and therefore was added,
     * false if the patient was already registered
     */
    public boolean addPatient(Patient patient){
        //adds the patient if it does not already exist in the patientsList
        if(!(allPatients.contains(patient))){
            allPatients.add(patient);
            return true;
        }
        return false;
    }

    /**
     * method to remove a patient from the patient register
     * @param patient - the patient that shall be removed
     * @return true if the patient existed in the register and therefore was removed,
     * false if the patient was not registered in the register and therefore could not be removed
     */
    public boolean removePatient(Patient patient){
        //returns true if the patient was removed from the allPatients list
        return allPatients.remove(patient);
    }
}
