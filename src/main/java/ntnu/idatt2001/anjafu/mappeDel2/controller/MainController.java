package ntnu.idatt2001.anjafu.mappeDel2.controller;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import ntnu.idatt2001.anjafu.mappeDel2.model.Patient;
import ntnu.idatt2001.anjafu.mappeDel2.view.PatientApp;
import ntnu.idatt2001.anjafu.mappeDel2.view.PatientDialog;

import java.util.Optional;

/**
 * controller class which handles communication between the view and model classes
 */
public class MainController {
    PatientApp app;

    /**
     * constructor that creates a new main controller
     * @param app - the application that created the new controller
     */
    public MainController(PatientApp app) {
        this.app = app;
    }

    /**
     * method to add a patient to the register
     */
    public void addPatient(){
        //creating a new "add patient" dialog window
        PatientDialog patientDialog = new PatientDialog();
        //showing the dialog window and waiting for the users respons
        Optional<Patient> result = patientDialog.showAndWait();

        //checking if the result is null or not (if null: user clicked on cancel button)
        if (result.isPresent()) {
            //if the result is present, it means the user clicked ok and wants to add the new patient
            Patient newPatient = result.get();
            if(!(app.getRegister().addPatient(newPatient))){ //returns false if the patient already exists
                showFailedAddPatientAlert(newPatient);
            }else{
                //updating the application to show the new information
                app.update();
            }
        }
    }

    /**
     * method to edit a chosen patient from the register
     * @param selectedPatient - the patient that will be edited
     */
    public void editPatient(Patient selectedPatient){
        if(selectedPatient != null){ //if the user clicked on a patient
            //creating a new "edit patient" dialog window
            PatientDialog employeeDialog = new PatientDialog(selectedPatient);
            //showing the dialog window and waiting for the users respons
            employeeDialog.showAndWait();
            //updating the application to show the (potentially) new information
            app.update();
        }else if(selectedPatient == null){ //if the user did not click on a patient
            showNoPatientSelectedAlert();
        }
    }

    /**
     * method to delete a chosen patient from the register
     * @param selectedPatient - the patient that will be removed
     */
    public void removePatient(Patient selectedPatient){
        if(selectedPatient != null){ //if the user clicked on a patient
            //checking whether the user actually wants to the delete the selected patient
            if(showDeleteConfirmationAlert(selectedPatient)){
                //removing the selected patient from the register
                app.getRegister().removePatient(selectedPatient);
                //updating the application to show the new information
                app.update();
            }
        }else if(selectedPatient == null){ //if the user did not click on a patient
            showNoPatientSelectedAlert();
        }
    }

    /**
     * method to show an alert when there is no chosen patient from the table
     */
    private void showNoPatientSelectedAlert(){
        //creating the warning alert
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Failure");
        alert.setHeaderText("No patient selected");
        //setting the content that is displayed inside the alert
        alert.setContentText("No patient has been selected from the table.\n"+"Please select a patient.");
        //showing the alert and waiting for the user to respond
        alert.showAndWait();
    }

    /**
     * method to display a delete confirmation window
     * @return true if the user confirms to delete the chosen patient
     */
    private boolean showDeleteConfirmationAlert(Patient patient) {
        boolean deleteConfirmed = false;

        //creating a new confirmation alert to check if the user wants to delete the patient
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete confirmation");
        alert.setHeaderText("Delete confirmation");
        //setting the content that is displayed inside the alert
        alert.setContentText("Are you sure you want to delete the patient "+patient.getFullName()+"?");

        //showing the alert and waiting for the user to respond
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            //if the user clicked on the ok button, then the delete action is confirmed
            deleteConfirmed = (result.get() == ButtonType.OK);
        }
        return deleteConfirmed;
    }

    /**
     * method to display a dialog box telling the user that the patient could not be added
     */
    private void showFailedAddPatientAlert(Patient patient){
        //creating a new confirmation alert to tell the user that the patient could not be added
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Failure");
        alert.setHeaderText("Failure in adding the patient");
        //setting the content that is displayed inside the alert
        String text = "The patient "+patient.getFullName()+" could not be added,\n because" +
                "there already exists a patient \nwith the social security number: "+patient.getSocialSecurityNumber();
        alert.setContentText(text);
        //showing the alert and waiting for the user to respond
        alert.showAndWait();
    }

    /**
     * method to show an alert window that contains information about the application
     */
    public void showAboutAlert(){
        //creating a new information alert
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Application information");
        alert.setHeaderText("Patients register\nv0.1-SNAPSHOT");
        //setting the content that is displayed inside the alert
        alert.setContentText("Application created by \nAnja Furholt\nanjafu@stud.ntnu.no");
        //showing the alert and waiting for the user to respond
        alert.showAndWait();
    }
}
