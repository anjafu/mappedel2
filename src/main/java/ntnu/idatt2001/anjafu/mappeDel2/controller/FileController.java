package ntnu.idatt2001.anjafu.mappeDel2.controller;

import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ntnu.idatt2001.anjafu.mappeDel2.model.CSVFileHandler;
import ntnu.idatt2001.anjafu.mappeDel2.view.PatientApp;
import java.io.File;
import java.io.IOException;

/**
 * controller class which handles file communication between the view and model classes
 */
public class FileController {
    PatientApp app;

    /**
     * constructor that creates a new file controller
     * @param app - the application that created the new controller
     */
    public FileController(PatientApp app) {
        this.app = app;
    }

    /**
     * method that opens and fills the appliaction with information from a csv file
     * @param stage - the stage that will show the filechooser
     */
    public void importFromCSV(Stage stage){
        //creating a new filechooser
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open CSV File");

        /*could have added extension filter making it impossible (on mac) to click on anything but
        csv files, but since exercise 4 asks to show a window when wrong file type is picked,
        i decided to not use the extension filter to fulfill this criteria.
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV", "*.csv"));*/

        //showing the filechooser and getting the file the user picked
        File file = fileChooser.showOpenDialog(stage);

        //if the file is not null, means the user clicked on a file
        if(file != null){
            if(getFileExtension(file).equalsIgnoreCase("csv")){ //if the file is a csv file
                try{
                    CSVFileHandler.readCSVFile(file, app.getRegister());
                }catch(IOException ioe){
                    //if an exception happens: showing this to the user so they know something went wrong
                    createExceptionAlert(ioe.getMessage());
                }
                //updating the application to show all the new information
                app.update();
            }else{
                //if the file does not have the extension "csv",
                // then telling the user they have to choose another file
                createWrongFileTypeAlert();
            }
        }
    }

    /**
     * method that creates a file with information from the application
     * @param stage - the stage that will show the filechooser
     */
    public void exportToCSV(Stage stage){
        //creating a new filechooser
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export to CSV file");
        //adding extensionfilter so (on mac) the file name is automatically ending with ".csv"
        //even if you remove the ".csv" part of the file, it is saved with ".csv" (on mac)
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV", "*.csv"));
        //showing the filechooser
        File file = fileChooser.showSaveDialog(stage);
        //if the file is not null, exporting all information to the file
        if (file != null) {
            try{
                CSVFileHandler.writeCSVFile(file, app.getRegister());
            }catch(IOException ioe){
                //if an exception happens: showing this to the user so they know something went wrong
                createExceptionAlert(ioe.getMessage());
            }
        }

        //dont have to implement the "if the file already exist" part of the assignment, since
        //filechooser already will tell you if the file already exists and then ask you if you
        //want to replace this file or not
    }

    /**
     * method that finds out what extension a file has
     * @param file - the file that shall be checked for its extension
     * @return - returns the extension as a string
     */
    private String getFileExtension(File file){
        String[] fileInfo = file.getName().split("\\.");
        //assuming its a regular file with a name and only one "."
        return fileInfo[1];
    }

    /**
     * method that creates an alert when wrong file type is chosen
     */
    private void createWrongFileTypeAlert(){
        //creating a new warning alert
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Wrong file type");
        alert.setHeaderText("Wrong file type");
        //setting the content that is displayed inside the alert
        alert.setContentText("The chosen file type is not valid. Please choose another file.");
        //showing the alert and waiting for the user to respond
        alert.showAndWait();
    }

    /**
     * method that creates an alert when an exception happens when opening/writing to a file
     * @param exceptionMessage - the message of the exception that happened
     */
    private void createExceptionAlert(String exceptionMessage){
        //creating a new warning alert
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Failure");
        alert.setHeaderText("Failure");
        //setting the content that is displayed inside the alert
        alert.setContentText("Something went wrong. Please contact anjafur@outlook.com with this message:\n+" +
                exceptionMessage);
        //showing the alert and waiting for the user to respond
        alert.showAndWait();
    }

}
