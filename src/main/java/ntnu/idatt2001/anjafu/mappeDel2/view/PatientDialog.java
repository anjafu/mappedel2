package ntnu.idatt2001.anjafu.mappeDel2.view;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import ntnu.idatt2001.anjafu.mappeDel2.model.NodeFactory;
import ntnu.idatt2001.anjafu.mappeDel2.model.Patient;

/**
 * class that represents one patient dialog window
 */
public class PatientDialog extends Dialog<Patient> {
    public enum Mode {
        NEW, EDIT
    }

    private final Mode mode;
    private Patient existingPatient = null;
    //creating all the nodes that will be shown in the gridpane
    TextField socialSecurityNumberField = (TextField) NodeFactory.create("textfield");
    TextField firstNameField = (TextField) NodeFactory.create("textfield");
    TextField lastNameField = (TextField) NodeFactory.create("textfield");
    TextField generalPractitionerField = (TextField) NodeFactory.create("textfield");
    TextField diagnosisField = (TextField) NodeFactory.create("textfield");
    //the string that shows what went wrong in case of invalid input
    String invalidInputExplanation;

    /**
     * constructor that creates a dialog window for adding a new patient
     */
    public PatientDialog() {
        super();
        mode = Mode.NEW;
        createDialog();
    }

    /**
     * constructor that creates a dialog window for editing a patient
     * @param existingPatient - the patient that will be edited
     */
    public PatientDialog(Patient existingPatient) {
        super();
        mode = Mode.EDIT;
        this.existingPatient = existingPatient;
        createDialog();
    }

    /**
     * method for creating the patient dialog window
     */
    private void createDialog(){
        //adding the buttons that will be shown in the dialog window
        super.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        //getting the ok button so i can add event filter to the button
        Button okBtn = (Button) super.getDialogPane().lookupButton(ButtonType.OK);
        //adding an event to the ok button to check if the input from the user is valid
        okBtn.addEventFilter(ActionEvent.ACTION, event -> {
            if(!(validateInput())) { //if the input is invalid
                event.consume(); //stopping the event (that way PatientDialog does not close)
                //creating and showing an alert that tells the user what went wrong
                showWarningAlert(invalidInputExplanation);
            }
        });

        //creating the gridpane that will be shown in the dialog window
        GridPane gp = (GridPane) NodeFactory.create("gridpane");
        gp.setHgap(10);
        gp.setVgap(10);
        gp.setPadding(new Insets(10, 10, 10, 10));

        //setting the prompt text inside the text fields
        socialSecurityNumberField.setPromptText("Social security number (11 digits)");
        firstNameField.setPromptText("First name");
        lastNameField.setPromptText("Last name");
        generalPractitionerField.setPromptText("General practitioner");
        diagnosisField.setPromptText("Diagnosis");

        //if the dialog is for editing, then one must fill all the fields with the patients information
        if ((mode == Mode.EDIT)) {
            //filling the dialog with the information about the patient
            firstNameField.setText(existingPatient.getFirstName());
            lastNameField.setText(existingPatient.getLastName());
            socialSecurityNumberField.setText(existingPatient.getSocialSecurityNumber());
            generalPractitionerField.setText(existingPatient.getGeneralPractitioner());
            diagnosisField.setText(existingPatient.getDiagnosis());
        }

        //adding all the nodes to the gridpane, with a label (text in front of textfield)
        gp.add(new Label("Name:*"), 0, 0);
        gp.add(firstNameField, 1, 0);
        gp.add(lastNameField, 2, 0);
        gp.add(new Label("Social security number:*"), 0, 1);
        gp.add(socialSecurityNumberField,1,1,2,1);
        gp.add(new Label("General practitioner:"),0,2);
        gp.add(generalPractitionerField,1,2,2,1);
        gp.add(new Label("Diagnosis:"),0,3);
        gp.add(diagnosisField,1,3,2,1);
        gp.add(new Label("* indicates required field."),1,4);

        //adding the gridpane to the content of the dialog window
        super.getDialogPane().setContent(gp);

        //making it not possible to click on other windows when the dialog window is showing
        super.initModality(Modality.APPLICATION_MODAL);

        //setting the title of the dialog window
        if(mode == mode.NEW) super.setTitle("Add new patient");
        else if(mode == mode.EDIT) super.setTitle("Edit patient");

        //setting what will be returned when the user clicks on one of the buttons
        setResultConverter((ButtonType button) -> {
            Patient result = null;
            if (button == ButtonType.OK) { //if the user presses the ok button
                if (mode == Mode.NEW) { //if this is a window for adding a new patient
                    //creating the new patient
                    result = new Patient(socialSecurityNumberField.getText().trim(),firstNameField.getText().trim(),
                            lastNameField.getText().trim(),generalPractitionerField.getText().trim(),diagnosisField.getText().trim());
                } else if (mode == Mode.EDIT) { //if this is a window for editing a patient
                    //updating all the information about the edited patient
                    existingPatient.setFirstname(firstNameField.getText().trim());
                    existingPatient.setLastName(lastNameField.getText().trim());
                    existingPatient.setSocialSecurityNumber(socialSecurityNumberField.getText().trim());
                    existingPatient.setGeneralPractitioner(generalPractitionerField.getText().trim());
                    existingPatient.setDiagnosis(diagnosisField.getText().trim());
                }
            }
            //returning the new patient or null if the user clicked on the cancel button/edited the patient
            return result;
        });
    }

    /**
     * method that checks whether the input in the text fields contain valid input
     * @return true if the input is valid, false if the input is not valid for editing/creating a new patient
     */
    private boolean validateInput(){
        invalidInputExplanation = "";
        boolean validInput = true;
        //checking if the text fields contain valid input
        if(firstNameField.getText().isBlank() || lastNameField.getText().isBlank()){
            invalidInputExplanation += "A patient must have a name, both first and last name.\n";
            validInput = false;
        }
        if(socialSecurityNumberField.getText().isBlank()){
            invalidInputExplanation += "A patient must have a social security number.\n";
            validInput = false;
        }
        if(!(socialSecurityNumberField.getText().trim().length() == 11)){
            invalidInputExplanation += "Social security numbers are 11 digits long.\n";
            validInput = false;
        }
        return validInput;
    }

    /**
     * method to create an alert box of type warning
     * @param invalidInputExplanation - feedback to the user on what went wrong
     */
    private void showWarningAlert(String invalidInputExplanation){
        //creating a new alert of type "warning"
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Invalid input");
        alert.setHeaderText("Invalid input!");
        //setting the content that is displayed inside the alert
        alert.setContentText(invalidInputExplanation);
        //showing the alert and waiting for the user to respond
        alert.showAndWait();
    }
}
