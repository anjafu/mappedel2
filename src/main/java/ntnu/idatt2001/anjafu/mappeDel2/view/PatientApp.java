package ntnu.idatt2001.anjafu.mappeDel2.view;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ntnu.idatt2001.anjafu.mappeDel2.controller.FileController;
import ntnu.idatt2001.anjafu.mappeDel2.controller.MainController;
import ntnu.idatt2001.anjafu.mappeDel2.model.Patient;
import ntnu.idatt2001.anjafu.mappeDel2.model.PatientRegister;
import ntnu.idatt2001.anjafu.mappeDel2.model.NodeFactory;

/**
 * class that creates the gui representation of the patient register app
 */
public class PatientApp extends Application {
    TableView<Patient> tableView;
    PatientRegister register;
    MainController controller;
    FileController fileController;
    Stage stage;

    /**
     * method that runs after the init method
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        //creating the layoutpane that will contain all the nodes
        BorderPane bp = (BorderPane) NodeFactory.create("borderpane");

        //Creating a vbox layoutpane that will fill the top of the borderpane
        /*It is necessary to cast the object since the VBox has methods that are not defined in
        the Node class*/
        VBox vb = (VBox) NodeFactory.create("vbox");

        tableView = createTableView(); //creating the tableview
        bp.setCenter(tableView); //adding the table to the center part of the borderpane

        MenuBar menuBar = createMenuBar(); //creating the menu bar
        vb.getChildren().add(menuBar); //adding the menubar to the vbox layoutpane

        ToolBar toolBar = createToolBar(); //creating the toolbar
        vb.getChildren().add(toolBar); //adding the toolbar to the vbox layoutpane

        //adding the vbox to the top of the main layoutpane (borderpane)
        bp.setTop(vb);

        //set title of the stage and add the root to the scene
        stage.setTitle("Patient overview application");
        stage.setScene(new Scene(bp, 900, 500));

        //showing the stage
        stage.show();
    }

    /**
     * method that runs when the application is opened
     * @throws Exception
     */
    @Override
    public void init() throws Exception {
        super.init();
        //creating instances of the controller and patient register
        controller = new MainController(this);
        fileController = new FileController(this);
        register = new PatientRegister();
    }

    /**
     * method that creates a new tableview (method only meant for this specific project)
     * @return the new tableview
     */
    private TableView<Patient> createTableView(){
        //creating the table to contain all patients
        TableView<Patient> tableView = (TableView<Patient>) NodeFactory.create("tableview");
        //creating all the columns in the table
        //not using the nodefactory since TableColumn does not extend the Node class
        TableColumn<Patient, String> nameColumn = new TableColumn<>("Name");
        TableColumn<Patient, String> firstNameColumn = new TableColumn<>("First name");
        TableColumn<Patient, String> lastNameColumn = new TableColumn<>("Last name");
        TableColumn<Patient, String> socialSecurityNumberColumn = new TableColumn<>("Social Security Number");
        TableColumn<Patient, String> generalPractitionerColumn = new TableColumn<>("General Practitioner");;
        TableColumn<Patient, String> diagnosisColumn = new TableColumn<>("Diagnosis");;
        //setting what the values of the columns will be
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        //adding the firstname and lastname columns to the name column to create nested columns
        nameColumn.getColumns().addAll(firstNameColumn,lastNameColumn);
        //adding all the columns to the table
        tableView.getColumns().addAll(nameColumn,socialSecurityNumberColumn,generalPractitionerColumn,diagnosisColumn);
        //making the table only show the columns that i made myself
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.setItems(FXCollections.observableArrayList(register.getPatients()));
        //returning the finished tableview
        return tableView;
    }

    /**
     * method that creates a new menubar (method only meant for this specific project)
     * @return the new menubar
     */
    private MenuBar createMenuBar(){
        //creating the menubar
        MenuBar menuBar = (MenuBar) NodeFactory.create("menubar");
        //creating all the menus belonging to the menu bar (the first thing you see)
        Menu fileMenu = new Menu("File");
        Menu editMenu = new Menu("Edit");
        Menu helpMenu = new Menu("Help");
        //creating the items that will be inside the menus
        MenuItem importFromCSV = new MenuItem("Import from .CSV");
        MenuItem exportToCSV =  new MenuItem("Export to .CSV");
        MenuItem addPatient =  new MenuItem("Add new patient");
        MenuItem editPatient =  new MenuItem("Edit selected patient");
        MenuItem removePatient =  new MenuItem("Remove selected patient");
        MenuItem about =  new MenuItem("About");
        //not adding the exit menu since it was not required

        //adding the menu items to their corresponding menu
        fileMenu.getItems().addAll(importFromCSV,exportToCSV);
        editMenu.getItems().addAll(addPatient,editPatient,removePatient);
        helpMenu.getItems().add(about);
        //adding shortcuts to specific menu items
        addPatient.setAccelerator(new KeyCodeCombination(KeyCode.A));
        editPatient.setAccelerator(new KeyCodeCombination(KeyCode.E));
        removePatient.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        //deciding what happens when you click on an item
        importFromCSV.setOnAction(event -> fileController.importFromCSV(stage));
        exportToCSV.setOnAction(event -> fileController.exportToCSV(stage));
        addPatient.setOnAction(event -> controller.addPatient());
        editPatient.setOnAction(event -> controller.editPatient(tableView.getSelectionModel().getSelectedItem()));
        removePatient.setOnAction(event -> controller.removePatient(tableView.getSelectionModel().getSelectedItem()));
        about.setOnAction(event -> controller.showAboutAlert());
        //adding the menus to the menu bar
        menuBar.getMenus().addAll(fileMenu,editMenu,helpMenu);
        //returning the finished menubar
        return menuBar;
    }

    /**
     * method that creates a new toolbar (method only meant for this specific project)
     * @return the new toolbar
     */
    private ToolBar createToolBar(){
        //creating the toolbar
        ToolBar toolBar = (ToolBar) NodeFactory.create("toolbar");
        //getting the images for the buttons
        Image addPatientImage = new Image("addIcon.png",25,20,true,false);
        Image editPatientImage = new Image("editIcon.png",25,20,true,false);
        Image deletePatientImage = new Image("deleteIcon.png",25,20,true,false);
        //creating the buttons inside the toolbar
        Button addPatientBtn = (Button) NodeFactory.create("button");
        addPatientBtn.setGraphic(new ImageView(addPatientImage));
        Button editPatientBtn = (Button) NodeFactory.create("button");
        editPatientBtn.setGraphic(new ImageView(editPatientImage));
        Button removePatientBtn = (Button) NodeFactory.create("button");
        removePatientBtn.setGraphic(new ImageView(deletePatientImage));
        //what happens when you click on the buttons
        addPatientBtn.setOnAction(event -> controller.addPatient());
        editPatientBtn.setOnAction(event -> controller.editPatient(tableView.getSelectionModel().getSelectedItem()));
        removePatientBtn.setOnAction(event -> controller.removePatient(tableView.getSelectionModel().getSelectedItem()));
        //adding the buttons to the toolbar
        toolBar.getItems().addAll(addPatientBtn,editPatientBtn,removePatientBtn);
        //returning the finished toolbar
        return toolBar;
    }

    /**
     * method that get the patient register
     * @return - the patient register
     */
    public PatientRegister getRegister() {
        return register;
    }

    /**
     * method that updates the table to show new/updated information
     */
    public void update(){
        //adding the changed register to the tableview
        tableView.setItems(FXCollections.observableArrayList(register.getPatients()));
        //refreshing the tableview to display the updated information
        tableView.refresh();
    }
}
