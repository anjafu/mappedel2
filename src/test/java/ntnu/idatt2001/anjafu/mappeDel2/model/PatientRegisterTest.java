package ntnu.idatt2001.anjafu.mappeDel2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test class for PatientRegister, because if this class does not work, nothing in the entire
 * application will work either (saving/reading file wont work if PatientRegister does not work, adding,
 * deleting and editing patient wont work either without PatientRegister)
 */
class PatientRegisterTest {
    PatientRegister register;
    Patient p1;
    Patient p2;
    Patient p3;

    @BeforeEach
    void initAll(){
        register = new PatientRegister();
        p1 = new Patient("12345678912","Anja","Fur","John","");
        p2 = new Patient("21987654321","Lise","Tom","John","");
        p3 = new Patient("12345678912","Ella","Lie","Smith","");
    }

    @Nested
    @DisplayName("Add patient")
    class AddPatientTest {
        @Test
        @DisplayName("Add not already existing patient")
        void addNotExistingPatient() {
            //adding a patient that has not been added before
            assertTrue(register.addPatient(p1));
            //adding a different patient that has not been added before
            assertTrue(register.addPatient(p2));
            //checking if the register has actually added the patients after using the addPatient method
            assertTrue(register.getPatients().contains(p1));
            assertTrue(register.getPatients().contains(p2));
        }

        @Test
        @DisplayName("Add already existing patient")
        void addExistingPatient() {
            //adding a patient
            register.addPatient(p1);
            //trying to add that patient again, which should not work (return false)
            assertFalse(register.addPatient(p1));
            //trying to add a patient with the same security number, which should not work (return false)
            assertFalse(register.addPatient(p3));
        }
    }

    @Nested
    @DisplayName("Remove patient")
    class RemovePatientTest {
        @Test
        @DisplayName("Remove already existing patient")
        void removeExistingPatient() {
            //adding a patient
            register.addPatient(p1);
            //trying to remove the patient that was added, which should return true
            assertTrue(register.removePatient(p1));
            //checking if the register contains the patient still, which should be false
            assertFalse(register.getPatients().contains(p1));
        }

        @Test
        @DisplayName("Remove a non-existent patient")
        void removeNotExistingPatient() {
            //trying to remove a patient that was never added, which should return false
            assertFalse(register.removePatient(p2));
        }
    }

}